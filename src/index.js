import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Acceuil from "./Acceuil/Acceuil";

// serviceWorkerRegistration.unregister();
if ("serviceWorker" in navigator) {
    console.log("true");
    let res = navigator.serviceWorker.register("./service-worker.js");
    console.log(res)
} else {
    console.log("false");
}
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Acceuil />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
