import {useState} from "react";
import {Box, Button, createTheme, Grid, TextField} from "@mui/material";
import {ThemeProvider} from "@emotion/react";
import './Accueil.css';

function Accueil() {
    //state(état ou données)
    const [films, setfilms] = useState([
            {img: "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg", nom: "star wars"},
            {img: "/9vaRPXj44Q2meHgt3VVfQufiHOJ.jpg", nom: "star trek"},
            {img: "/wuMc08IPKEatf9rnMNXvIDxqP4W.jpg", nom: "harry potter"}
        ]
    );
    const [title, setTitle] = useState("");


    //comportement
    const handleClick = () => {
        fetch(
            "https://api.themoviedb.org/3/search/movie?api_key=b6670df8de5c84b7341d6a5d02d653c1&query=" + title)
            .then((res) => res.json())
            .then((json) => {
                let tempoArray = [];
                json.results.map((film) => (
                    // console.log(film["original_title"])
                    tempoArray.push({
                        nom: film["original_title"],
                        img: film["poster_path"]
                    })
                ))
                setfilms(tempoArray);
            })
    }

    const handleTitleChange = event => {
        // 👇️ access textarea value
        setTitle(event.target.value);
    };


    const theme = createTheme({
        palette: {
            neutral: {
                main: '#64748B',
                contrastText: '#fff',
            },
        },
    });


    //render
    return <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{minHeight: '100vh'}}
    >
        <ThemeProvider theme={theme}>
            <img src={require('../Icons/clap.png')} alt="clap"/>
            <h1>Quoi regarder ?</h1>
            <TextField sx={{m: 2}} inputProps={{style: {fontFamily: 'Arial', color: 'white'}}} color={'neutral'}
                       id="title"
                       label="Titre du film"
                       variant="standard"
                       value={title}
                       onChange={handleTitleChange}
                       focused/>
            <Button onClick={handleClick} sx={{m: 2}} variant="contained" color="neutral">Rechercher</Button>

            <ul>
                {films.map((film) => (
                    <li>{film.nom}</li>
                ))}
            </ul>
            <div>
                {films.map((film) => (
                    <Box component="div" sx={{display: 'inline'}}>
                        <img src={"https://image.tmdb.org/t/p/w500" + film.img} alt="clap"/>
                    </Box>
                ))}
            </div>

        </ThemeProvider>
    </Grid>
}

export default Accueil;
